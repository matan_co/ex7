<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170609_185430_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(45),
            'password' => $this->string(45),
            'auth_key' => $this->string(45),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
