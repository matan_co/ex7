<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170609_192306_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'genre' => $this->string(45),
            'min_age' => $this->integer(),
            'grade' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
